%!TEX root = ../../predoc-plain.tex

\tikzstyle{vertex_set1} = [%
  circle,%
  minimum size=25px,%
  fill=MaterialBlue100,%
  draw=MaterialBlue700,%
  text=MaterialBlue800,%
  scale=1];

\tikzstyle{vertex_set2} = [%
  circle,%
  minimum size=25px,%
  fill=MaterialGreen200,%
  draw=MaterialGreen800,%
  text=MaterialGreen900,%
  scale=1];

\tikzstyle{vertex_set3} = [%
  circle,%
  minimum size=25px,%
  fill=MaterialYellow200,%
  draw=MaterialYellow800,%
  text=MaterialYellow900,%
  scale=1];

\tikzstyle{edge} = [%
  draw=MaterialBlue700,%
  line width=1px];

\tikzstyle{fill_1a} = [%
  fill,%
  pattern=north west lines,%
  pattern color=MaterialBlue100%
  ];
\tikzstyle{fill_1b} = [%
  fill,%
  pattern=north east lines,%
  pattern color=MaterialBlue100%
  ];
\tikzstyle{fill_2} = [%
  fill,%
  pattern=crosshatch,%
  pattern color=MaterialYellow100%
  ];

% Hatches
\draw[fill_1a] (0:1.25) -- (60:1.25) -- (120:1.25) -- (180:1.25) -- cycle;
\draw[fill_1b] (180:1.25) -- (240:1.25) -- (300:1.25) -- (360:1.25) -- cycle;
\draw[fill_2] (0:1.25) -- (60:1.25) -- (30:2.25) -- cycle;
\draw[fill_2] (60:1.25) -- (120:1.25) -- (90:2.25) -- cycle;
\draw[fill_2] (120:1.25) -- (180:1.25) -- (150:2.25) -- cycle;
\draw[fill_2] (180:1.25) -- (240:1.25) -- (210:2.25) -- cycle;
\draw[fill_2] (240:1.25) -- (300:1.25) -- (270:2.25) -- cycle;
\draw[fill_2] (300:1.25) -- (360:1.25) -- (330:2.25) -- cycle;
  
% Vertices
\node (v0)  at   (0:1.25) [vertex_set1] {$v_0$};
\node (v1)  at  (60:1.25) [vertex_set1] {$v_1$};
\node (v2)  at (120:1.25) [vertex_set1] {$v_2$};
\node (v3)  at (180:1.25) [vertex_set1] {$v_3$};
\node (v4)  at (240:1.25) [vertex_set1] {$v_4$};
\node (v5)  at (300:1.25) [vertex_set1] {$v_5$};

\node (v6)  at  (30:2.25) [vertex_set3] {$v_6$};
\node (v7)  at  (90:2.25) [vertex_set3] {$v_7$};
\node (v8)  at (150:2.25) [vertex_set3] {$v_8$};
\node (v9)  at (210:2.25) [vertex_set3] {$v_9$};
\node (v10) at (270:2.25) [vertex_set3] {$v_{10}$};
\node (v11) at (330:2.25) [vertex_set3] {$v_{11}$};

% Edges
\draw [-,edge] (v0) to (v1);
\draw [-,edge] (v1) to (v2);
\draw [-,edge] (v2) to (v3);
\draw [-,edge] (v3) to (v4);
\draw [-,edge] (v4) to (v5);
\draw [-,edge] (v5) to (v0);

\draw [-,edge] (v0) to (v6);
\draw [-,edge] (v0) to (v11);
\draw [-,edge] (v1) to (v6);
\draw [-,edge] (v1) to (v7);
\draw [-,edge] (v2) to (v7);
\draw [-,edge] (v2) to (v8);
\draw [-,edge] (v3) to (v8);
\draw [-,edge] (v3) to (v9);
\draw [-,edge] (v4) to (v9);
\draw [-,edge] (v4) to (v10);
\draw [-,edge] (v5) to (v10);
\draw [-,edge] (v5) to (v11);

\draw [-,edge,dashed] (v0) to (v3);
