% !TEX root = ../predoc-plain.tex
%
\chapter{%
  \texorpdfstring%
  {Sucesiones-$f$ y gr\'afica de $hongo$-trayectorias}%
  {Sucesiones-f y gr\'afica de hongo-trayectorias}%
} % (fold)
\label{cha:sucesiones_f_y_grafica_de_hongo_trayectorias}

En este cap\'itulo, presentamos las \aclp{fs} como una forma de representaci\'on de familias de ciclos hamiltonianos en el hipercubo.
Complementariamente a este concepto, describimos un conjunto de acciones que dan una primera noci\'on de equivalencia entre estas sucesiones, con lo que se fija una partici\'on en el conjunto de \acp{fs} y se generan las primeras clases de equivalencia.
Considerando el orden lexicogr\'afico de las sucesiones, denominamos como los representantes de cada clase a las \emph{m\'inimas}.

Definimos una nueva relaci\'on de equivalencia entre las \acp{fs}, especialmente entre sus representantes, que llamamos \emph{hongo}.
Definimos una gr\'afica que tiene como v\'ertices a las \acp{fs} representantes y una arista en esta existe, si y s\'olo si, dos \acf{fs} est\'an relacionados por el \emph{hongo}.
Llamamos a estas gr\'aficas de \emph{hongo}-trayectorias.

Construimos una biblioteca de funciones de c\'omputo para generar y procesar \acp{fs}.
En una primera etapa, programamos funciones que permitieron resolver algunos problemas de decisi\'on y de b\'usqueda, por ejemplo: la b\'usqueda del conjunto de \acp{fs} de un orden determinado, la b\'usqueda del representante m\'inimo dada una \ac{fs}, la decisi\'on de si una \ac{fs} es la m\'inima en su clase o de buena formaci\'on de una \ac{fs}, entre otras m\'as.

En una segunda etapa y con la definici\'on de las gr\'aficas de \emph{hongo}-trayectorias, programamos funciones que permitieron la construcci\'on de estas gr\'aficas y definimos una descripci\'on de estas que permitieron su almacenaje en disco.

Cabe se\~nalar que la representaci\'on de cada v\'ertice, la cardinalidad del conjunto de v\'ertices y del conjunto de aristas tienen un crecimiento superior al exponencial respecto al orden de la \ac{fs} lo que implic\'o una dificultad pr\'actica.
Lo anterior, permiti\'o construir evidencia experimental de un par instancias \emph{peque\~nas} de las gr\'aficas de \emph{hongo}-trayectorias y permiti\'o observar que estas gr\'aficas son conexas.

Con base en esto, planeamos la conjetura sobre la conexidad de las \emph{hongo}-trayectorias generadas a partir de los hipercubos.
Esto implica que es posible transformar cualquier ciclo hamiltoniano en otro y una forma es mediante alguna composiciones de la acciones de equivalencia y la relación \emph{hongo}.

\section{%
  \texorpdfstring%
  {Sucesiones-$f$}%
  {Sucesiones-f}%
} % (fold)
\label{sec:sucesiones_f}

\begin{note}
  \label{not:vertices_como_vectores}
  En esta secci\'on, es conveniente denotar a los v\'ertices del hipercubo como una tupla con lo que se mantiene consistencia con la notaci\'on particular de la~\autoref{def:hypercube}.
\end{note}

El hipercubo es una gr\'afica hamiltoniana y el n\'umero de ciclos hamiltonianos en esta gr\'afica tiene un crecimiento doblemente exponencial respecto a su dimensi\'on~\autocite{douglas1977bounds}.

\begin{notation}
  Denotemos con ${\{0,\dots,n-1\}}^{\ast}$ a la colecci\'on de sucesiones de longitud finita con t\'erminos en el conjunto de \'indices $\{0,\dots,n-1\}$.
\end{notation}

\begin{definition}
  Una \emph{\ac{fs} de orden $n$} con longitud $m \leq 2^n$ es una sucesi\'on finita $F = [a_0,\dots,a_{m-1}] \in \intv{n}^m$, tal que:
  \begin{itemize}
    \item si $m < 2^n$, entonces en cada subsucesi\'on contigua propia de $F$, $[a_k, \dots, a_l]$, con $0 \leq k < l \leq m$, existe al menos un valor que aparece un n\'umero impar de veces en la subsucesi\'on observada,
    \item mientras que, si $m = 2^n$, entonces en todo valor en $F$ aparece un n\'umero par de veces en esta y cualquier subsucesi\'on contigua propia de $F$ es una sucesi\'on-$f$ en si misma.
  \end{itemize}

  Por lo tanto, por definici\'on, la longitud m\'axima de una \ac{fs} de orden $n$ es $2^n$ y, en este caso, decimos que la \ac{fs} es \emph{completa}.
\end{definition}

\begin{figure}[H]
  \begin{subfigure}[t]{.45\linewidth}
    \centering
    \begin{tikzpicture}
      \input{gfx/tikz/exmp-fseq_in_q3.tikz}
    \end{tikzpicture}
    \caption{El ciclo hamiltoniano $h=v_0v_1v_3v_2v_6v_7v_5v_4v_0$ en $Q_3$}
    \label{fig:ejemplo-ciclo_hamiltoniano_en_3cubo}
  \end{subfigure}%
  \hfill
  \begin{subfigure}[t]{.45\linewidth}
    \centering
    \begin{tikzpicture}
      \input{gfx/tikz/exmp-fseq_in_q3-2.tikz}
    \end{tikzpicture}
    \caption{La \acl{fs} $0~1~0~2~0~1~0~2$ que describe al ciclo hamiltoniano $h$}
    \label{fig:ejemplo-sucf_describe_ciclo_hamiltoniano_en_3cubo}
  \end{subfigure}%
  \hfill
  \caption{Ejemplo de una \acl{fs} en el hipercubo de dimensi\'on $3$ como representante de ciclos hamiltonianos}
  \label{fig:ejemplo_sucf_en_3cubo}
\end{figure}

\begin{definition}
  Dado un punto (v\'ertice) fijo $\vec{v}_0 \in V{(Q_n)}$, sea:
  \[
    T_{\vec{v}_0} : \intv{n}^{\ast} \to Q_n
  \]
  la \emph{funci\'on de trayectoria} definida recursivamente:
  \begin{align}
    T_{\vec{v}_0}{(\mbox{\tt nil})} & = \vec{v}_0
    \nonumber
    \\
    T_{\vec{v}_0}(Aa) & = T_{\vec{v}_0}(A) + \vec{e}_a
    \nonumber
  \end{align}
\end{definition}

Intuitivamente, para cada \ac{fs} $A \in \intv{n}^{\ast}$, $T_{\vec{v}_0}(A)$ es el punto alcanzado en el hipercubo por la trayectoria que comienza en el punto base $\vec{v}_0$ y se mueve seg\'un las entradas de $A$.

\begin{remark}
  Si $A$ es una \ac{fs} de longitud $m<2^n$, entonces $T_{\vec{v}_0}(A) \neq \vec{v}_0$.
\end{remark}

De hecho, dado que un \'indice $a \in \intv{n}$ aparece un n\'umero impar de veces,
$T_{\vec{v}_0}(A)-\vec{v}_0$ no es cero en la entrada $a$-\'esima.
Por consiguiente:

\begin{remark}%
  \label{rem:hamiltonian_cycle}
  Si $F$ es una \ac{fs} completa, entonces la aplicaci\'on de la funci\'on $T$ con $F$ partiendo del v\'ertice $\vec{v}_0$
  \[
    T_{\vec{v}_0}(F)=\vec{v}_0
  \]
  describe un ciclo hamiltoniano.
  Este ciclo tiene inicio en $\vec{v}_0$ y contin\'ua de acuerdo a la sucesi\'on de aristas correspondientes a cada coordenada indicada por $F$.
\end{remark}

\begin{definition}%
  \label{def:equivalence_of_sequences}
  Decimos que dos \acp{fs} completas, $F_i$ y $F_j$, son \emph{equivalentes}, si y s\'olo si, ambas representan los mismos ciclos hamiltoninos de acuerdo con la~\autoref{rem:hamiltonian_cycle}, sin considerar los puntos de partida o el sentido de las trayectorias.
  Una trayectoria tambi\'en se identifica con su reverso.
  Denotamos por $F_i \equiv F_j$ a la equivalencia de estas \acp{fs}.
\end{definition}

\begin{definition}
  \label{def:fs_less_than}
  Dadas dos \acp{fs} completas, $F_i$ y $F_j$, decimos que $F_i$ es menor a $F_j$ bajo el orden lexicogr\'afico y lo denotamos como $F_i < F_j$.
\end{definition}

\begin{definition}
  \label{def:minimal_fs}
  La \emph{\ac{fs} m\'inima} de una clase de equivalencia de acuerdo a la~\autoref{def:equivalence_of_sequences} es aquella que no tienen otra \ac{fs} menor a ella dentro de su clase.
  \[
    F \mbox{ es m\'inima}
    \iff
    \neg \exists F'
    \; : \;
    (F' \equiv F)
    \wedge
    (F' < F)
  \]
\end{definition}

\begin{example}
  Consideremos, el conjunto de \acp{fs} de orden $2$, compuesto solamente por las \acp{fs}: $F_1 = 0~1~0~1$ y $F_2 = 1~0~1~0$.
  Es f\'acil notar que ambas representan los mismos ciclos hamiltonianos, por ende, $F_1 \equiv F_2$.
  Luego entonces, decimos que $F_1 < F_2$ considerando el orden lexicogr\'afico, sin embargo, $F_1$ es la \ac{fs} m\'inima en su clase.
\end{example}

\begin{definition}%
  \label{def:alternative_sequences}
  Decimos que dos \acp{fs}, $F_i$ y $F_j$, son \emph{alternativas} si dado un punto base, $\vec{v}_0$, ellas describen trayectorias que alcanzan el mismo punto final, es decir, cumplen que:
  \[
    F_i \alt F_j
    \iff
    \forall \vec{v}_0 \in Q^n \; : \; T_{\vec{v}_0}{(F_i)} = T_{\vec{v}_0}{(F_j)}
  \]
  Evidentemente, $\alt$ es una relaci\'on de equivalencia.
\end{definition}

\begin{definition}%
  \label{def:ocurrences_count_convention}
  Dada cualquier subsucesi\'on $A = [a_i,\dots,a_j]$ de una \ac{fs} completa $F$ y cualquier \'indice $a \in \intv{n}$, denotaremos con $\occu{a}{A}$ al n\'umero de ocurrencias de $a$ en $A$.

  Entonces, $\forall a \in \intv{n}$, $\occu{a}{F} \equiv 0 \pmod{2}$.
\end{definition}

\begin{definition}%
  \label{def:type_of_a_sequence}
  Sea $F = [a_0,\dots,a_{2^n - 1}]$ una \ac{fs} completa.
  El \emph{tipo de una subsucesi\'on} $[a_i,\dots,a_j]$ of $F$, con $0 \leq i < j < 2^n$, denotada por $\type{a_i,\dots,a_j}$, es el conjunto de valores en $[a_i,\dots,a_j]$ que aparecen un n\'umero impar de veces.

  El \emph{tipo de una \ac{fs} completa} es el conjunto vac\'io.
\end{definition}

\begin{remark}%
  \label{rem:endpoints_trajectory_difference}
  % Consideremos una subsucesi\'on propia con entradas extremos $a_i$ y $a_j$ en una \ac{fs} de orden $n$ y suponga que $\type{a_i, \dots, a_j} = \{a\}$, entonces
  % \[
  %   \forall \vec{v}_0 \in Q^n \; : \; T_{\vec{v}_0}{(a_i \cdots a_j)} = \vec{v}_0 + \vec{e}_a,
  % \]
  % es decir, los extremos de la trayectoria difieren exactamente en la $a$-\'esima entrada.
  Consideremos una subsucesi\'on propia con entradas extremas $a_i$ y $a_j$ en una \ac{fs} de orden $n$, si
  \[
    \type{a_i,\dots,a_j} = \{b_1,\dots,b_l\}
  \]
  entonces,
  \[
    T_{\vec{v}_0}(a_i,\dots,a_j) = x_0 + x_{b_1} + \dots + x_{b_l}.
  \]
\end{remark}

\begin{definition}%
  % [The complementary subsequence]
  \label{def:type_of_comp_of_seq}
  Sea $F = AB$ una \ac{fs} completa que se puede descomponer en las subsucesiones propias no vac\'ias $A$ y $B$.
  Decimos que $A$ es la sucesi\'on complementaria de $B$ y viceversa.
\end{definition}

\begin{notation}
  Considerando la instancia de~\autoref{def:type_of_comp_of_seq}, denotamos a las subsucesiones complementarias como $A^c = B$ y $B^c = A$.
\end{notation}

\begin{remark}%
  \label{rem:type_of_complementary_sequence}
  El tipo de $A$ es igual al tipo de $A^c$:
  \begin{equation}
    \type{A} = \type{A^c}.
  \end{equation}
\end{remark}

\begin{proof}
  Claramente,
  \begin{align}
    a \in \type{A}
    & \iff
    \occu{a}{A} \equiv 1 \pmod{2}
    \nonumber
    \\
    & \stackrel{*}{\iff}
    \occu{a}{A^c} \equiv 1 \pmod{2}
    \nonumber
    \\
    &\iff
    a \in \type{A^c},
  \end{align}
  la equivalencia $\ast$ se cumple debido a $\occu{a}{F} \equiv 0 \pmod{2}$, planteado en la~\autoref{def:ocurrences_count_convention}.
\end{proof}

%
% section sucesiones_f (end)
%

\begin{remark}
  \label{rm.Aaa}
  Sea $F$ una \ac{fs} completa y $A a$ una subsucesi\'on propia de $F$.
  Si $\type{A a} = \{a'\}$, entonces necesariamente $a \neq a'$.
\end{remark}

\begin{proof}
  Procedemos por contradicci\'on:
  \begin{align}
    \label{eq:a_eq_ap_imp_a_notin_type_A}
    a = a'
    & \implies
    \type{A a} = \{a\}
    \nonumber
    \\
    & \implies \occu{a}{Aa} \equiv 1 \pmod{2}
    \nonumber
    \\
    & \implies \occu{a}{A} \equiv 0 \pmod{2}
    \nonumber
    \\
    & \implies a \notin \type{A}.
  \end{align}
  Para cualquier otro \'indice $d \neq a$, si $d \in \type{A}$, entonces necesariamente $d \in \type{A a}$, pero esto no es posible debido a que $\type{Aa} = \{a\}$.
  As\'i entonces $\type{A} = \emptyset$ contradice que $A$ es una \ac{fs} contenida propiamente en $F$.
\end{proof}

\section{Relaci\'on hongo} % (fold)
\label{sec:relacion_hongo}

En esta secci\'on presentamos una transformaci\'on de \acp{fs}, que nombramos \emph{hongo}, y que establece una relaci\'on de equivalencia entre \acp{fs}.
En la \autoref{sec:sucesiones_f} presentamos noci\'on de equivalencia que genera clases de \acp{fs} en hipercubos de dimensi\'on mayor o igual a $2$.
Con esta relaci\'on \emph{hongo}, aplicable a \acp{fs} de orden mayor $3$, ampliamos la noci\'on de equivalencia previa presentada.

\begin{definition}%
  \label{def:msh_precond}
  Supongamos una \ac{fs} completa $F$ que se divide como $F = A a B c C b$, donde $A,B,C$ son subsucesiones propias no vac\'ias de $F$ y $a,b,c \in \intv{n}$.

  Definimos el predicado:
  \begin{align}
    \label{eq:msh_precondition_predicate}
    \Phi(A a B c C b\,;\,a',b',c')
    \equiv
    & \;(\type{A a} = \{a'\})
    \;\wedge
    \nonumber
    \\
    & \;(\type{a B c} = \{b'\})
    \;\wedge
    \nonumber
    \\
    & \;(\type{c C} = \{c'\}),
  \end{align}
  donde $a',b',c' \in \intv{n}$.
  A este predicado $\Phi$ le llamamos \emph{precondici\'on hongo}.
\end{definition}

\begin{example}
  Consideremos $F = 0~1~0~2~0~1~0~3~0~1~0~2~0~1~0~3$, una \ac{fs} completa de orden $4$.
  Una instancia que satisface el predicado $\Phi$ es $A = 0~1$, $a = 0$ $B = 2~0~1~0~3~0~1~0~2$, $c = 0$, $C = 1~0$ y $b = 3$ y, por lo tanto, $a' = 1$, $b' = 3$, $c' = 1$ y una \ac{fs} resultante es $F' = 0~1~3~1~0~1~2~0~1~0~3~0~1~0~2~1$ (ver~\autoref{fig:msh_precond_exmp}).
  \[
    \Phi(\underbrace{0~1}_{A} \underbrace{0}_{a} \underbrace{2~0~1~0~3~0~1~0~2}_{B} \underbrace{0}_{c} \underbrace{1~0}_{C} \underbrace{3}_{b}\,;\,\underbrace{1}_{a'},\underbrace{3}_{b'},\underbrace{1}_{c'})
  \]
\end{example}

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[scale=0.5]
    \input{gfx/tikz/msh_precond_exmp.tikz}
  \end{tikzpicture}
  \caption{\texorpdfstring%
    {Ejemplo gr\'afico de la satisfacci\'on del predicado $\Phi$}%
    {Ejemplo gr\'afico de la satisfacci\'on del predicado Phi}}
  \label{fig:msh_precond_exmp}
\end{figure}

\begin{remark}%
  \label{rem:type_equiv_msh_trns}
  De la~\autoref{def:msh_precond} y la~\autoref{rem:type_of_complementary_sequence} se puede ver que:
  \begin{align}
    \label{eq:type_A_a_eq_type_B_c_C_b}
    \type{A a} & = \type{B c C b}\\
    \label{eq:type_a_B_c_eq_type_C_b_A}
    \type{a B c} & = \type{C b A}\\
    \label{eq:type_c_C_eq_type_b_A_a_B}
    \type{c C} & = \type{b A a B}
  \end{align}
\end{remark}

\begin{remark}%
  \label{rem:different_edge_indices_in_mushroom}
  Se cumple que:
  \begin{equation}
    \Phi(A a B c C b\,;\, a',b',c')
    \implies
    \left(a \not \in \{a',b'\} \right)
    \wedge
    \left(b \not \in \{a',c'\} \right)
    \wedge
    \left(c \not \in \{b',c'\} \right).
  \end{equation}
\end{remark}

\begin{proof}
  Supongamos $\Phi(A a B c C b\,;\, a',b',c')$.

  \begin{enumerate}[I)]
    \item
    %
    % Demostracion: a \neq b'
    %
    Primero, demostremos $a \notin \{a',b'\}$.

    De la~\autoref{rm.Aaa} sabemos $a \neq a'$ y $a \neq b'$ dado que $\type{Aa} = \{a'\}$ y que $\type{a B c} = \{b'\}$, respectivamente.

    \item
    %
    % Demostracion: b \neq a'
    %
    Luego, demostremos $b \notin \{a',c'\}$.

    Dado que ${(A a)}^c = B c C b$, sabemos que
    \[
      \type{B c C b} = \type{A a} = \{a'\},
    \]
    entonces por la~\autoref{rm.Aaa} sabemos que $b \neq a'$.

    %
    % Demostracion: b \neq c'
    %
    Por otro lado, de la ~\autoref{rem:type_of_complementary_sequence} sabemos que
    \[
      \type{b A a B} = \type{{(c C)}^c} = \{c'\}
    \] y por consiguiente de la~\autoref{rm.Aaa} sabemos $b \neq c'$.

    \item
    %
    % Demostracion: c \neq c'
    %
    Finalmente, demostremos $c \notin \{b',c'\}$.

    Por la~\autoref{rm.Aaa} sabemos que $\type{a B c} = \{b'\}$ y que $\type{c C} = \{c'\}$ y por lo tanto, sabemos $c \neq b'$ y $c \neq c'$ respectivamente.
  \end{enumerate}
\end{proof}

En las siguientes observaciones, supondremos una \ac{fs} completa $F$ que se divide como $F = A a B c C b$ y que cumple con $\Phi(A a B c C b\,;\, a',b',c')$, para algunos $a',b',c' \in \intv{n}$.

\begin{remark}%
  \label{lem:type_A_type_B_type_C}
  Se cumplen los tipos:
  \begin{align}
    \label{eq:type_A}
    \type{A} & = \{a,a'\}
    \\
    \label{eq:type_B}
    \type{B} & =
    \begin{cases}
      \{b'\} & a = c \\
      \{a,b',c\} & a \neq c
    \end{cases}
    \\
    \label{eq:type_C}
    \type{C} & = \{c,c'\}
  \end{align}
\end{remark}

\begin{proof}
  \begin{enumerate}[I)]
  \item
  Demostremos la~\autoref{eq:type_A}.

  Primero observemos que $a \neq a'$ s\'olo porque $\type{A a} = \{a'\}$ y la~\autoref{rm.Aaa}.
  Por lo tanto, $\occu{a}{Aa} \equiv 0 \pmod{2}$, lo que implica que $\occu{a}{A} \equiv 1 \pmod{2}$ y $a \in \type{A}$.

  Adem\'as $\occu{a'}{Aa} \equiv 1 \pmod{2}$, lo que implica que $\occu{a'}{A} \equiv 1 \pmod{2}$ y $a' \in \type{A}$.

  No existe un tercer \'indice $e \notin \{a,a'\}$ pero s\'i en $\type{A}$, porque de otro modo $e \in \type{Aa}$ contradice $\type{A a} = \{a'\}$.
  \medskip

  \item
  Demostremos la~\autoref{eq:type_B}.

  Dado que $\type{a B c} = \{b'\}$, por la~\autoref{rm.Aaa} sabemos $a \neq b'$ and $c \neq b'$.

  Si $a = c$, entonces $\type{B} = \type{a B a} = \{b'\}$.

  En cambio, si $a \neq c$, entonces dado que $\type{a B c} = \{b'\}$, sabemos que $\occu{a}{a B c} \equiv 0 \pmod{2}$ y por lo tanto $\occu{a}{B} \equiv 1 \pmod{2}$ y $a \in \type{B}$.
  Del mismo modo, se puede ver que $c \in \type{B}$.
  Ning\'un cuarto \'indice puede pertenecer a $\type{B}$.
  \medskip

  \item
  Demostremos la~\autoref{eq:type_C}.

  Dado que $\type{c C} = \{c'\}$ por la~\autoref{rm.Aaa} sabemos $c \neq c'$.
  As\'i que $c \notin \type{c C}$, espec\'ificamente $\occu{c}{c C} \equiv 0 \pmod{2}$ y por lo tanto $\occu{c}{C} \equiv 1 \pmod{2}$ y $c \in \type{C}$.
  Es evidente $c' \in \type{C}$ tambi\'en y ning\'un tercer \'indice puede pertenecer a $\type{C}$.
  \end{enumerate}
\end{proof}

Ahora, mencionamos algunas consecuencias con respecto a la relaci\'on \emph{alternativa} introducida en la~\autoref{def:alternative_sequences}.

\begin{notation}%
  \label{not:reverse_of_an_f_sequence}
  Dada una \ac{fs} $F = [a_k,\dots,a_l]$ con $0 \leq k < l \leq 2^n - 1$, denotamos la \emph{sucesi\'on en orden reverso} de $F$, $[a_l,\dots,a_k]$, con $F^r$.
\end{notation}

\begin{remark}%
  \label{rem:alternative_sequences}
  Continuando con la consideraci\'on que $F = AaBcCb$, por un lado:
  \begin{align}
    A & \; \alt \; b C^r c B^r a
    \nonumber
    \\
    & \; \alt \; b C^r b'
    \nonumber
    \\
    & \; \alt \; b c' B^r a
    \nonumber
    \\
    & \; \alt \; b c' c b'
    \nonumber
    \\
    & \; \alt \; a' a
    \nonumber
    \\
    & \; \alt \; a' B c b'
    \nonumber
  \end{align}
  por otro lado (en un sentido sim\'etrico)
  \begin{align}
    C & \; \alt \; c B^r a A^r b
    \nonumber
    \\
    & \; \alt \; b' A^r
    \nonumber
    \\
    & \; \alt \; c B^r  a' b
    \nonumber
    \\
    & \; \alt \; b' a a' b
    \nonumber
    \\
    & \; \alt \; c c'
    \nonumber
    \\
    & \; \alt \; c B^r a' b
    \nonumber
  \end{align}
  y tambi\'en
  \begin{align}
    B & \; \alt \; a A^r b C^r c
    \nonumber
    \\
    & \; \alt \; a b' C c'
    \nonumber
    \\
    & \; \alt \; a' b C^r c
    \nonumber
    \\
    & \; \alt \; a' b c'
    \nonumber
  \end{align}
\end{remark}

Todas estas relaciones son evidentes en la~\autoref{fig:msh_exmp}.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[scale=0.5]
    \input{gfx/tikz/msh_exmp.tikz}
  \end{tikzpicture}
  \caption{Ejemplo de un hongo}
  \label{fig:msh_exmp}
\end{figure}

De las Observaciones~\ref{rem:different_edge_indices_in_mushroom}--\ref{rem:alternative_sequences} pueden surgir varias comparaciones entre los conjuntos $\{a,b,c\}$ and $\{a',b',c'\}$.

En un enfoque puramente te\'orico establecido, podemos considerar la~\autoref{def:nonadj_prop_seq_type}.

\begin{definition}%
  \label{def:nonadj_prop_seq_type}
  Supongamos una \ac{fs} completa $F = ABCD$ donde $A,B,C,D$ son subsucesiones propias no vac\'ias de $F$.
  El \emph{tipo} de las sucesiones propias no contiguas $A$ y $C$ es la diferencia sim\'etrica de sus \emph{tipos} propios.
  Lo denotamos con $\type{A|C}$.
  \begin{align}
    \type{A|C} & = \type{A} \triangle~\type{C}
    \nonumber
    \\
    & = \left(\type{A} - \type{C}\right) \cup \left(\type{C} - \type{A}\right)
  \end{align}
\end{definition}

\begin{notation}%
  \label{not:sets_of_f_sequences}
  Denotamos con $\mathbb{F}_{n}$ al conjunto de \acp{fs} completas de orden $n$ y con $\mathbb{F}^{\ast}_{n}$ al conjunto de \acp{fs} completas m\'inimas de orden $n$.

  Se puede ver que $\mathbb{F}^{\ast}_{n} \subset \mathbb{F}_{n}$.
\end{notation}
\begin{definition}%
  \label{def:msh_relation}
  Sean $F$ y $F'$ dos \acp{fs} completas de orden $n$ construidas con el procedimiento:
  \begin{description}
  \item[Si ] $F$ puede ser dividida como $F = A a B c C b$, donde $A,B,C$ son subsucesiones propias no vac\'ias de $F$ y $a,b,c \in \intv{n}$, {\bf y} adem\'as existen $a',b',c' \in \intv{n}$ tales que cumplen $\Phi(A a B c C b\,;\, a',b',c')$,
  \item[entonces ] $F' = A b' C c' B^r a'$,
  \item[de otro modo ] $F' = F$.
  \end{description}

  Decimos que \emph{$F$ est\'a relacionada con $F'$ mediante la transformaci\'on hongo}.
\end{definition}

\begin{notation}%
  \label{not:orbit_of_mushroom}
  Denotamos con $\mathcal{M}_n$ al conjunto de parejas de \acp{fs} de mismo orden relacionadas mediante la transformaci\'on \emph{hongo}.
  Se puede ver que $\mathcal{M}_n \subset \mathbb{F}_n \times \mathbb{F}_n$.
\end{notation}

\begin{remark}
  Si $(F,F') \in \mathcal{M}_n$, entonces o bien $F = F'$ o $F'$ es una \ac{fs} no equivalente a $F$.
\end{remark}

\begin{remark}%
  \label{rem:index_convention}
  En la~\autoref{def:msh_precond}, bien puede suceder que la precondici\'on $\Phi(A a B c C b\,;\, a',b',c')$ para la relaci\'on \emph{hongo} se satisfaga en varias formas.
  Si $F = A a B c C b$ y $F'$ es la \ac{fs} correspondiente construida de acuerdo con la~\autoref{def:msh_relation}, entonces escribiermos $F' = \mathtt{Hongo}{(F,i_a,i_b,i_c)}$
  s\'olo para enfatizar que $a$, $b$ y $c$ aparecen en las posiciones  $i_a,i_b,i_c \in \intv{2^n}$, respectivamente.
\end{remark}

%
% section relacion_hongo (end)
%

\section{\texorpdfstring%
  {Gr\'aficas de \emph{hongo}-trayectorias $P_n$}
  {Gr\'aficas de hongo-trayectorias}} % (fold)
\label{sec:grafica_de_hongo_trayectorias}

\begin{definition}%
  \label{def:msh_tray_graph}
  La \emph{gr\'afica de hongo-trayectorias} $P_n = (\mathbb{F}_n, \mathcal{M}_n)$ es aquella gr\'afica cuyos v\'ertices son las \acp{fs} completas de orden $n$ y sus aristas son parejas de \acp{fs} relacionadas por $\mathcal{M}_n$.

  La \emph{gr\'afica m\'inima de hongo-trayectorias} $P^{\ast}_n = P_n[\mathbb{F}^{\ast}_n]$ es la subgr\'afica inducida por el conjunto de \acp{fs} m\'inimas de orden $n$.

\end{definition}

\begin{conjecture}%
  \label{conj:msh_tray_graph_is_connected}
  La gr\'afica $P^{\ast}_n$ es conexa.
\end{conjecture}

Todos los ciclos hamiltonianos en el hipercubo est\'an particionados mediante la noci\'on de equivalencia introducida en la~\autoref{def:equivalence_of_sequences}.
As\'i entonces, dos ciclos hamiltonianos en de la misma clase de equivalencia se pueden relacionar mediante alguna composici\'on de acciones de esta equivalencia.
Por otro lado, la relaci\'on \emph{hongo}, introducida en la~\autoref{def:msh_relation}, relaciona los ciclos hamiltonianos de dos clases de equivalencia distintas.
Finalmente, la~\autoref{conj:msh_tray_graph_is_connected} propone que esta relaci\'on \emph{hongo} efectivamente relaciona a todas las clases de equivalencia, es decir, que es posible encontrar una composici\'on de esta relaci\'on entre cualesquiera dos representantes de distintas clases de equivalencia.

Debido al r\'apido crecimiento del orden y tama\~no de las gr\'aficas $P^{\ast}_n$ respecto a $n$ fue inviable la construcci\'on de estas gr\'aficas con valores de $n$ mayores a $5$, a su vez, hace necesario optar por otra perspectiva de estudio.

\begin{note}
  Las gr\'aficas $P_n$ y $P^{\ast}_n$ son seudogr\'aficas dirigidas.
  Con el prop\'osito de simplificar el an\'alisis, consideramos una versi\'on simplificada de estas gr\'aficas.
  Si una arista en $P^{\ast}_n$ est\'a formada por una pareja $F_i,F_j$, es decir, $F_j$ se obtiene de aplicar la transformaci\'on \emph{hongo} a $F_i$, entonces en la versi\'on simplificada, la arista no dirigida $F_i,F_j$ existe.
\end{note}

\begin{definition}
  Dada una gr\'afica $P^{\ast}_n$, la gr\'afica simplificada $P^{+}_n$ es aquella gr\'afica simple no dirigida que tiene el mismo conjunto de v\'ertices $V{(P^{\ast}_n)}$.
  Una arista dirigida $F_i,F_j \in E{(P^{\ast}_n)}$ implica la existencia de la arista no dirigida $F_i,F_j \in E{(P^{+}_n)}$.
\end{definition}

%
% section grafica_de_hongo_trayectorias (end)
%

\section{Algoritmos} % (fold)
\label{sec:algoritmos_sucesiones_f}

En esta secci\'on, usaremos la siguiente notaci\'on.

\begin{notation}
  Si $F$ es una \ac{fs} completa e $i \in \intv{2^n}$, entonces la $i$-\'esima entrada en $F$ la denotamos con $F[i]$.
  De manera similar, la subsucesi\'on de $F$ correspondiente a los \'indices inclusivos desde $i$ a $j$: $F{[\![i,,j]\!]} = \left[F_{\iota}\right]_{\iota=i}^j$, la denotamos con $F{[\![i,j]\!]}$ donde $i < j$.
\end{notation}

Introducimos procedimientos efectivos para decidir, dada una \ac{fs} completa $F$, si el siguiente objetivo es alcanzado:
\begin{equation}
  \left. \mbox{
    \begin{minipage}{.85\textwidth}
    \begin{description}
    \item[Objetivo: ] $F$ se divide como $F = A a B c C b$ tal que cumple $\Phi(A a B c C b\,;\, a',b',c')$ para $a',b',c' \in \intv{n}$.
    \end{description}
    \end{minipage}
  }\ \right\}
  \label{eq.goal}
\end{equation}

Dado que los c\'alculos de los tipos de subsecuencias est\'an involucrados, el primer procedimiento de b\'usqueda, es seleccionar las entradas en una subsecuencia que aparece un n\'umero impar de veces.
Esto se realiza de manera bastante directa mediante el procedimiento que se muestra en el~\autoref{alg:type_of_an_f_sequence_search}.
Considerando la~\autoref{def:type_of_comp_of_seq}, este procedimiento decide, en la condici\'on de la~\autoref{alg:type_of_an_f_sequence_search_2}, cu\'al es la subsucesi\'on m\'as corta para el conteo, es decir, o bien $F{[\![i,j]\!]}$ o su complemento en $F$.
Observamos que la~\autoref{alg:type_of_an_f_sequence_search_9} tiene el prop\'osito de agregar una entrada $a$ en el conjunto de salida de acuerdo con su m\'odulo $2$.
Este es un algoritmo con complejidad de tiempo $O(j-i) = O(2^n)$.

\begin{algorithm}[H]
  \DontPrintSemicolon
  \SetKwIF{SSi}{EnOtroCasoSi}{EnOtroCaso}{si}{entonces}{si no, si}{si no}{fin si}
  \Entrada{%
    $F$, una \ac{fs} completa de orden $n$,\\%
    $i,j \in \intv{2^n}$, dos \'indice de $F$, tales que $i < j$}
  \Salida{$P$, el conjunto de entradas $\sigma$ en $F{[\![i,j]\!]}$ que aparecen un n\'umero impar de veces}
  \BlankLine
  $P \gets \emptyset$
  \tcp*[r]{Conjunto para $\type{F{[\![i,j]\!]}}$}
  \SSi(\tcp*[f]{$F{[\![i,j]\!]}$ es m\'as corto}){$(j-i) \leq (2^n + i -j)$}{
    \label{alg:type_of_an_f_sequence_search_2}
    $\alpha \gets i$
    \label{alg:type_of_an_f_sequence_search_3}
    \tcp*[r]{Tomar $i$ como \'indice inicial}
    $\beta \gets j-i$
    \label{alg:type_of_an_f_sequence_search_4}
    \tcp*[r]{Fijar $j-i$ entradas a contar}
  }
  \EnOtroCaso(\tcp*[f]{Complemento ${F{[\![i,j]\!]}}^c$ es m\'as corto}){
    $\alpha \gets j$
    \label{alg:type_of_an_f_sequence_search_6}
    \tcp*[r]{Tomar $j$ como \'indice inicial}
    $\beta \gets 2^n + i - j$
    \label{alg:type_of_an_f_sequence_search_7}
    \tcp*[r]{Fijar $2^n + i - j$ entradas a contar}
  }
  \ParaCada(\tcp*[f]{Contar $\beta$ entradas desde \'indice $\alpha$}){$l \gets 0$ \KwA $\beta$}{
    \label{alg:type_of_an_f_sequence_search_9}
    $a \gets F[{(l + \alpha) \mod 2^n}]$
    \tcp*[r]{Valor en $a$-\'esima posici\'on en $F$}
    $P \gets P \triangle \{a\}$
    \tcp*[r]{Alternar valor en $a$}
  }
  \Devolver{$P$}
  \tcp*[r]{$\type{F{[\![i,j]\!]}}$ computado}
  \caption{\texorpdfstring%
  {B\'usqueda del tipo de una \ac{fs} - \texttt{parity}}%
  {B\'usqueda del tipo de una sucesi\'on-f - parity}}
  \label{alg:type_of_an_f_sequence_search}
\end{algorithm}

En el~\autoref{alg:mshdfsalg}, mostramos un procedimiento de b\'usqueda exhaustivo para calcular el conjunto de instancias de hongo dada una \ac{fs} completa de orden $n$.

Este procedimiento busca tres \'indices $i_a$, $i_b$, $i_c$, tales que satisfagan la precondici\'on \emph{hongo} planteada en la~\autoref{def:msh_precond}.
Dado que $A$, $B$ y $C$ son subsucesiones no vac\'ias, la distancia entre cualquier par de \'indices $i_a$, $i_b$ o $i_c$ debe ser mayor a $1$.

El \'indice $i_a$ no tiene restricciones y cualquiera de las $2^n$ posiciones son v\'alidas.

Una vez que $i_a$ es fijado, el \'indice $i_c$ tiene tres restricciones:
\begin{inparaenum}
  \item no ser contiguo a $i_a$ \emph{por la derecha},
  \item dejar al menos una posici\'on v\'alida para el \'indice $i_b$, que ser\'a el siguiente a fijar, y
  \item no ser contiguo a $i_b$ \emph{por la izquerda};
\end{inparaenum}
por lo tanto, tienen $2^n - 4$ posiciones v\'alidas.

Finalmente, una vez que $i_c$ es fijado, el \'indice $i_b$ puede ser fijado en todas las posiciones \emph{a la derecha} de $i_c$ y \emph{a la izquierda} de $i_a$, excluyendo las posiciones contiguas a dichos \'indices.

\begin{algorithm}[H]
  \DontPrintSemicolon
  \SetKwFunction{Parity}{tipo}
  \SetKwFunction{KwCont}{continue}
  \Entrada{%
    $F$, una \acl{fs} completa de orden $n$}
  \Salida{%
    $\Sigma = \{\sigma \; | \; \sigma = (i_a,i_b,i_c,a',b',c')\}$, el conjunto de instancias de aplicaci\'on \emph{hongo} en $F$}
  \BlankLine
  $\Sigma \gets \emptyset$
  \label{alg:mshdfsalg_1}
  \tcp*[r]{Conjunto de instances $\sigma$}
  \ParaCada(){$i_a \gets 0$ \KwA $2^n - 1$}{
    \ParaCada(){$i_c \gets i_a + 2$ \KwA $2^n + i_a - 4$}{
      $P \gets \Parity(F,i_a,i_c)$
      \tcp*[r]{Computa $\type{F{[\![i_a,i_c]\!]}}$}
      \SSi(\tcp*[f]{Tipo no unitario}){$|P| \neq 1$}{
        \KwCont
        \tcp*[r]{Descarta $i_c$}
      }
      $b' \in P$
      \tcp*[r]{Conservar $b'$ v\'alida}
      \BlankLine
      \ParaCada(){$i_b \gets i_c + 2$ \KwA $2^n + i_a - 2$}{
        $P \gets \Parity(F,i_c,i_b-1)$
        \tcp*[r]{Computa $\type{F{[\![i_c,i_b-1]\!]}}$}
        \SSi(\tcp*[f]{Tipo no unitario}){$|P| \neq 1$}{
          \KwCont
          \tcp*[r]{Descarta $i_b$}
        }
        $c' \in P$
        \tcp*[r]{Conservar $c'$ v\'alida}
        \BlankLine
        $P \gets \Parity(F,i_b+1,i_a)$
        \tcp*[r]{Computa $\type{F{[\![i_b+1,i_a]\!]}}$}
        \SSi(\tcp*[f]{Tipo no unitario}){$|P| \neq 1$}{
          \KwCont
          \tcp*[r]{Descarta $i_b$}
        }
        $a' \in P$
        \tcp*[r]{Conservar $a'$ v\'alida}
        \BlankLine
        $\Sigma \gets \Sigma \cup \{(i_a,i_b,i_c,a',b',c')\}$
        \tcp*[r]{Guardar instancia $\sigma$}
      }
    }
  }
  \Devolver{$\Sigma$}
  \tcp*[r]{Conjunto de $\Sigma$ encontrado}
  \caption{B\'usqueda de particiones $\Sigma$ en una \acl{fs} - \texttt{msh\_dfs}}
  \label{alg:mshdfsalg}
\end{algorithm}

El~\autoref{alg:mshdfsalg} realiza una b\'usqueda sin informaci\'on (a ciegas) previa sobre los componentes internos de las \ac{fs}, por lo que debe buscar cada posici\'on v\'alida para los \'indices, de ah\'i la b\'usqueda exhaustiva de las subsucesiones $A$, $B$ y $C$.

Ahora, supongamos que la posiciones de alg\'un \'indice se conoce antes de la b\'usqueda.
Definamos una partici\'on de $F$ como $\delta_1 : A a B c C b \mapsto A b' C c' B^r a'$.
Esto implica que la subsucesi\'on $A$ empeza en la posici\'on $0$, la subsucesi\'on $C$ termina en la posici\'on $2^n - 2$ y el \'indice $i_b$ est\'a fijo en la posici\'on $2^n - 1$ en $F$.

En el~\autoref{alg:mshdfsalg_delta1}, mostramos un procedimiento de b\'usqueda de la partici\'on $\Sigma_{\delta_1}$: busca todas las particiones existentes en una \ac{fs} de orden $n$.
La complejidad en tiempo del caso promedio es la misma que el caso peor y es igual a $O\left((2^n)^2\right) = O(4^n)$.

\begin{algorithm}[H]
  \DontPrintSemicolon
  \SetKwFunction{Parity}{tipo}
  \SetKwFunction{KwCont}{continue}
  \Entrada{%
    $F$, una \acl{fs} de orden $n$}
  \Salida{%
    $\Sigma = \left\{ \sigma | \sigma = (i_a,i_b,i_c,a',b',c') \right\}$: el conjunto de instancias de aplicaci\'on \emph{hongo} en $F$ caracterizados por la partici\'on $\delta_1$}
  \BlankLine
  $\Sigma \gets \emptyset$\;
  \label{alg:mshdfsalg_delta1_1}
  $i_b \gets 2^n-1$
  \tcp*[r]{$i_b$ es constante}
  \ParaCada(){$i_a \gets 1$ \KwA $2^n-5$}{
    $P \gets \Parity(F,0,i_a)$
    \tcp*[r]{Computa $\type{F{[\![0,i_a]\!]}}$}
    \SSi(\tcp*[f]{Tipo no unitario}){$|P| \neq 1$}{
      \KwCont
      \tcp*[r]{Descarta $i_a$}
    }
    $a' \in P$
    \tcp*[r]{Conservar $a'$ v\'alida}
    \BlankLine
    \ParaCada(){$i_c \gets i_a + 2$ \KwA $2^n  - 3$}{
      $P \gets \Parity(F,i_a,i_c)$
      \tcp*[r]{Computa $\type{F{[\![i_a,i_c]\!]}}$}
      \SSi(\tcp*[f]{Tipo no unitario}){$|P| \neq 1$}{
        \KwCont
        \tcp*[r]{Descarta $i_b$}
      }
      $b' \in P$
      \tcp*[r]{Conservar $b'$ v\'alida}
      \BlankLine
      $P \gets \Parity(F,i_c,i_b-1)$
      \tcp*[r]{Computa $\type{F{[\![i_c,i_b-1]\!]}}$}
      \SSi(\tcp*[f]{Tipo no unitario}){$|P| \neq 1$}{
        \KwCont
        \tcp*[r]{Descarta $i_c$}
      }
      $c' \in P$
      \tcp*[r]{Conservar $c'$ v\'alida}
      \BlankLine
      $\Sigma \gets \Sigma \cup \{(i_a,i_b,i_c,a',b',c')\}$
      \label{alg:mshdfsalg_delta1_line_18}
      \tcp*[r]{Guarda instancia $\sigma_{\delta_1}$}
    }
  }
  \Return{$\Sigma$}
  \tcp*[r]{Conjunto de $\Sigma_{\delta_1}$ encontrado}
  \caption{B\'usqueda de particiones $\Sigma_{\delta_1}$ en una \acl{fs} - \texttt{msh\_dfs\_delta1}}
  \label{alg:mshdfsalg_delta1}
\end{algorithm}

%
% section algoritmos_sucesiones_f (end)
%

\section{Contribuciones} % (fold)
\label{sec:contribuciones_sucesiones_f}

\subsection{Instancias peque\~nas de gr\'afica de \emph{hongo}-trayectoria} % (fold)
\label{sub:instancias_pequenas_grafica_hongo_trayectoria}

Los conjuntos $\mathbb{F}^{\ast}_n$ para $n = 2,3$ son conjuntos unitarios.
Estos son los conjuntos de v\'ertices de las gr\'aficas $P^{\ast}_n$.
Al ser gr\'aficas con un s\'olo v\'ertices no aportan informaci\'on \'util para el estudio de estas gr\'aficas, particularmente su conexidad.

Para $n = 4,5$, hemos computado las gr\'aficas $P^{\ast}_n$ respectivas.
La cardinalidad de sus conjuntos de v\'ertices, es decir, el orden de las gr\'aficas es $9$ y $237675$~\cite{dejter2007classes}, respectivamente, y \'estas instancias ya aportan informaci\'on para estudiar su conexidad.

Hemos computado el histograma de grados de entrada ($deg^-$) y grados de salida ($deg^+$) de estas gr\'aficas $P^{\ast}_n$.
Como se puede ver en la~\autoref{fig:fs-n4-msh-degs-hist}, el histograma de la distribuci\'on de  los grados en $P^{\ast}_4$ podr\'ian tener una distribuci\'on uniforme o normal; sin una tendencia clara.
Considerando el tama\~no tan peque\~no de $P^{\ast}_4$, $81$, no es factible aproximarlo a ninguna distribuci\'on y cualquier intento de ajuste resulta in\'util e insuficiente para dar estimados precisos.

Por otro lado, el histograma de grados para $P^{\ast}_5$ muestra una curva mejor definida que aparenta ser aproximable a una distribuci\'on normal.
Esta instancia tiene un valor $p$ de $2.75 \times {10}^{-4}$ y un valor estad\'istico de $65.\bar{6}$ en una prueba $\chi^2$.

\begin{table}[tb]
  \centering
  \begin{tabular}{|c|c|c|c|c|}
  \hline
  &
  \multicolumn{2}{c|}{$P^{\ast}_4$} &
  \multicolumn{2}{c|}{$P^{\ast}_5$}
  \\
  &
  grado de entrada & % deg^- in P^*_4
  grado de salida  & % deg^+ in P^*_4
  grado de entrada & % deg^- in P^*_5
  grado de salida   % deg^+ in P^*_5
  \\
  \hline
  $\min{(\deg)}$ &
  $2$ & % deg^- in P^*_4
  $4$ & % deg^+ in P^*_4
  $1$ & % deg^- in P^*_5
  $4$   % deg^+ in P^*_5
  \\
  $\text{media}{(\deg)}$ &
  $9.89$ & % deg^- in P^*_4
  $9.89$ & % deg^+ in P^*_4
  $26.7$ & % deg^- in P^*_5
  $26.7$   % deg^+ in P^*_5
  \\
  $\text{des. est.}{(\deg)}$ &
  $9.17$ & % deg^- in P^*_4
  $3.78$ & % deg^+ in P^*_4
  $4.13$ & % deg^- in P^*_5
  $3.8$    % deg^+ in P^*_5
  \\
  $\max{(\deg)}$ &
  $34$ & % deg^- in P^*_4
  $16$ & % deg^+ in P^*_4
  $53$ & % deg^- in P^*_5
  $48$   % deg^+ in P^*_5
  \\
  \hline
  $\text{media}{(\#\deg)}$ &
  $0.2812$    & % deg^- in P^*_4
  $9.\bar{8}$ & % deg^+ in P^*_4
  $4570.6730$ & % deg^- in P^*_5
  $4570.6730$   % deg^+ in P^*_5
  \\
  $\text{mediana}{(\#\deg)}$ &
  $0$     & % deg^- in P^*_4
  $0$     & % deg^+ in P^*_4
  $349.5$ & % deg^- in P^*_5
  $66.5$    % deg^+ in P^*_5
  \\
  $\max{(\#\deg)}$ &
  $0$     & % deg^- in P^*_4
  $0$     & % deg^+ in P^*_4
  $349.5$ & % deg^- in P^*_5
  $66.5$    % deg^+ in P^*_5
  \\
  $\mbox{Unif }\chi^2 stat$ &
  $44.\bar{3}$ & % deg^- in P^*_4
  $65.\bar{6}$ & % deg^+ in P^*_4
  $645911.66$  & % deg^- in P^*_5
  $678198.86$    % deg^+ in P^*_5
  \\
  $\mbox{Unif }\chi^2 p\text{-value}$ &
  $5.7074 \times {10}^{-2}$ & % deg^- in P^*_4
  $2.7526 \times {10}^{-4}$ & % deg^+ in P^*_4
  $0$                       & % deg^- in P^*_5
  $0$                         % deg^+ in P^*_5
  \\
  $\mbox{Norm }\chi^2 stat$ &
  $22.8327$ & % deg^- in P^*_4
  $56.2761$ & % deg^+ in P^*_4
  $18.3583$ & % deg^- in P^*_5
  $18.6114$   % deg^+ in P^*_5
  \\
  $\mbox{Norm }\chi^2 p\text{-value}$ &
  $1.1013 \times {10}^{-5}$  & % deg^- in P^*_4
  $6.0226 \times {10}^{-13}$ & % deg^+ in P^*_4
  $1.0316 \times {10}^{-4}$  & % deg^- in P^*_5
  $9.0903 \times {10}^{-5}$    % deg^+ in P^*_5
  \\
  \hline
  \end{tabular}
  \caption{\texorpdfstring%
  {Datos estad\'isticos de la distribuci\'on de grado de las gr\'aficas $P^{\ast}_4$ y $P^{\ast}_5$}%
  {Datos estad\'isticos de la distribuci\'on de grado de las gr\'aficas P*4 y P*5}}%
  \label{tab:msh_degs_hist_stats}
\end{table}

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.7\linewidth}
    \centering
    \includegraphics[width=\textwidth]{gfx/png/fs-n4-msh-degs-hist.png}
    \caption{\texorpdfstring%
      {Histograma para $P^{\ast}_{4}$}%
      {Histograma para P*4}%
    }
    \label{fig:fs-n4-msh-degs-hist}
  \end{subfigure}%
  \\
  \begin{subfigure}[b]{0.7\linewidth}
    \centering
    \includegraphics[width=\textwidth]{gfx/png/fs-n5-msh-degs-hist.png}
    \caption{\texorpdfstring%
      {Histograma para $P^{\ast}_{5}$}%
      {Histograma para P*5}%
    }
    \label{fig:fs-n5-msh-degs-hist}
  \end{subfigure}
  \caption{\texorpdfstring%
    {Histograma de la distribuci\'on de grado en $P^{\ast}_{n}$}%
    {Histograma de la distribuci\'on de grado en P*n}%
  }
  \label{fig:fs-n-msh-degs-hist}
\end{figure}

%
% subsection instancias_pequenas_grafica_hongo_trayectoria (end)
%

\subsection{Instancias grandes de la gr\'afica de \emph{hongo}-trayectoria} % (fold)
\label{sub:instancias_grandes_grafica_hongo_trayectoria}

Llamamos \emph{instancias grandes} de $P^{\ast}_n$, a aquellas con $n > 5$ debido a que estas gr\'aficas tienen un conjunto grande de v\'ertices.

El c\'omputo de estas gr\'aficas no es viable por su tama\~no y orden.
El n\'umero de v\'ertices crece r\'apidamente a medida que $n$ se incrementa.

La gr\'afica $P^{\ast}_6$ tiene, de hecho, aproximadamente $7.7 \times {10}^{17}$ v\'ertices~\cite{haanpaa2014counting}.

%
% subsection instancias_grandes_grafica_hongo_trayectoria (end)
%

%
% section contribuciones_sucesiones_f (end)
%

%
% chapter sucesiones_f_y_grafica_de_hongo_trayectorias (end)
%
