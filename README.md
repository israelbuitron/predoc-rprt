# Reporte Predoctoral

Reporte predoctoral de mi tesis.

![Coverage](https://gitlab.com/gitlab-org/gitlab/badges/master/coverage.svg?job=coverage&style=flat)

## &iquest;C&oacute;mo est&aacute;n organizados los archivos?

El c&oacute;digo fuente de este proyecto est&aacute; escrito mayormente en LaTeX.

Los archivos importantes son:

-  `predoc-plain.tex` - Archivo principal del c&oacute;digo fuente del documento.
-  `predoc-plain-refs.bib` - Archivo de las referencias, en formato Biblatex, requeridas en el documento.
-  `contenido/` - Directorio de archivos TEX que forman el contenido del documento.
   -  `contenido/capitulo-introduccion.tex` - Archivo del Capítulo 1 _"Introducci&oacute;n"_.
   -  `contenido/capitulo-conceptos.tex` - Archivo del Capítulo 2 _"Conceptos b&aacute;sicos de teor&iacute;a de gr&aacute;ficas"_.
   -  `contenido/capitulo-graficastrayectorias.tex` - Archivo del Capítulo 3 _"Gr&aacute;ficas de trayectorias y problemas de la madeja"_.
   -  `contenido/capitulo-hongotrayectorias.tex` - Archivo del Capítulo 4 _"Sucesiones-f y gr&aacute;ficas de hongo-trayectorias"_.
-  `gfx/` - Directorio de im&aacute;genes usadas en el documento.
   -  `gfx/pdf` - Recursos PDF.
   -  `gfx/png` - Recursos PNG.
   -  `gfx/tikz` - Recursos TIKZ.

## &iquest;Por qu&eacute; lo publico?

Quiero dejar disponible mi reporte predoctoral para **permitir que cualquier persona pueda leer mi trabajo y, si as&iacute; lo considera, realizar cualquier tipo de contribuci&oacute;n** (cr&iacute;tica, correcci&oacute;n, sugerencia, entre otras).

Creo que el conocimiento deber&iacute;a socializarse, principalmente los trabajos y las investigaciones realizadas por quienes nos hemos formado en las instituciones de educaci&oacute;n p&uacute;blica.

## &iquest;C&oacute;mo puedes contribuir?

### Contribuye opinando

Puedes **leer mi reporte** predoctoral y **compartirme tus comentarios**, sugerencias o cr&iacute;ticas.
**Toda contribuci&oacute;n** que tenga el prop&oacute;sito de mejorar mi trabajo **es bien recibida y agradecida**.

Puedes **opinar de cualquier aspecto**: ideas del trabajo, mejoras metodol&oacute;gicas de la investigaci&oacute;n, sugerencias de experimentos, mejoras al c&oacute;digo de LaTeX, etc.

### Contribuye comparti&eacute;ndome modificaciones del c&oacute;digo fuente

Dado que este reporte se construye mediante c&oacute;digo de LaTeX cualquier persona lo puede reconstruir, es decir, compilar el c&oacute;digo y generar el PDF del documento; as&iacute; entonces, si realizas alguna mejora al c&oacute;digo fuente, puedes tambi&eacute;n compart&iacute;rmela mediante un _merge-request_.

## Preguntas Frecuentes

### &iquest;C&oacute;mo compartirme tus opiniones?

Existen varias formas y las enlisto en mi preferencia:

1.  Puedes **enviarme tus opiniones mediante la secci&oacute;n [_Issues_][1]** que hay en la p&aacute;gina de este repositorio.
    Esta es la forma que prefiero porque me permite darle un seguimiento m&aacute;s p&uacute;blico, transparente y preciso.
2.  Puedes **enviarme un correo electr&oacute;nico, un tuit, un mensaje, etc**. y ponerte en contacto de esa forma.
    Este medio puede ser m&aacute;s sencillo para ti, pero me implica un poco m&aacute;s de trabajo a mi, sin embargo, por favor que esto no te desaliente a comunicarte conmigo.

Cualquiera que sea el medio que eligas, te lo agradezco.

### &iquest;C&oacute;mo utilizo _Issues_?

Sigue estos pasos:

1.  Ve a la secci&oacute;n [_Issues_][1].
2.  Presiona el bot&oacute;n _New Issues_.
3.  Llena el formulario con los detalles de tu opini&oacute;n.
    Completar este paso genera un registro llamado _ticket_, mismo que puedo darle seguimiento y, tanto t&uacute; como cualquier persona, puede observar como evoluciona.

### &iquest;Qu&eacute; es un _merge-request_?

Es una solicitud para incorporar alguna modificaci&oacute;n al proyecto principal.

Imagina que t&uacute; realizas alguna mejora al c&oacute;digo fuente, lo que implica alguna modificaci&oacute;n.
Esta modificaci&oacute;n existir&aacute; en tu computadora, m&aacute;s prescisamente en el repositorio donde est&eacute;s trabajando una copia de mi c&oacute;digo.
Hasta este punto no puedo saber de la existencia de esa modificaci&oacute;n tuya.
Para que yo la conozca debes compartirme tus modificaci&oacute;nes mediante una solicitud, que en el contexto de los repositorios GIT, se conoce como _merge-request_.

## Contacto

Estas son algunos medios con los que puedes ponerte en contacto conmigo, as&iacute; que puedes sentirte en completa libertad de hacerlo.

-  Twitter: [@IsraelBuitronD](https://twitter.com/IsraelBuitronD "Mi cuenta en Twitter")
-  Instagram: [@IsraelBuitronD](https://instagram.com/IsraelBuitronD "Mi cuenta en Instagram")
-  Facebook: [@IsraelBuitronCrypto](https://facebook.com/IsraelBuitronCrypto "Mi p&aacute;gina en Facebook")
-  P&aacute;gina web: [www.computacion.cs.cinvestav.mx/~ibuitron](www.computacion.cs.cinvestav.mx/~ibuitron "Mi p&aacute;gina web como estudiante de doctorado")
-  Correo electr&oacute;nico: [ibuitron@cinvestav.mx](mailto:ibuitron@cinvestav.mx "Mi correo electr&oacute;nico")

[1]: https://gitlab.com/israelbuitron/predoc-rprt/issues